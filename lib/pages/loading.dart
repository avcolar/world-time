import 'package:flutter/material.dart';
import 'package:flutter_playground/service/world_time.dart';

class Loading extends StatefulWidget {
  @override
  _LoadingState createState() => _LoadingState();
}

class _LoadingState extends State<Loading> {

  String time = 'loading';


  void setWorldTime() async {

    WorldTime worldTime = WorldTime();
    await worldTime.getTime();

    setState(() {
      time = worldTime.time;
    });
  }

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Text('loading'),
    );
  }
}
